use hotel

db.rooms.insert({
	name: "Single",
	accomodates: 2,
	price: 1000,
	description: "A simple room with all the basic necessities",
	rooms_available: 10,
	isAvailable: false
})

db.rooms.insertMany([
	{
		name: "Double",
		accomodates: 3,
		price: 2000,
		description: "A room fit for a small family going on a vacation",
		rooms_available: 5,
		isAvailable: false
	},
	{
		name: "Queen",
		accomodates: 4,
		price: 4000,
		description: "A room with a queen sized bed perfect for a simple getaway",
		rooms_available: 15,
		isAvailable: false
	}
])

db.rooms.find({name: "Double"})

db.rooms.updateOne(
	{_id : ObjectId("6287800eac99bdbba0d7f474")},
	{
		$set: {
			rooms_available: 0
		}
	}
)

db.rooms.deleteMany({
	rooms_available:0
})

